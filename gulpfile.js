import gulp from 'gulp';
import gulpSass from "gulp-sass";
import nodeSass from "sass";
import cleanCSS from 'gulp-clean-css';
import clean from "gulp-clean";
import autoPrefixer from 'gulp-autoprefixer';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import imageMin from 'gulp-imagemin';



import BS from 'browser-sync'



const sass = gulpSass(nodeSass);
const browserSync = BS.create();
const { src, dest, task, watch, series } = gulp;


const cleanDist = () => {
	return src('dist', { read: false })
        .pipe(clean());
}


const convertCss = () => src('src/sass/**/*')
	.pipe(sass())
	.pipe(concat('main.min.css'))
	.pipe(cleanCSS())
	.pipe(autoPrefixer({
        cascade: false
    }))
	.pipe(dest('dist/styles'));


    
    


const convertJs = () => src('src/js/**/*')
   .pipe(concat('scripts.min.js'))
   .pipe(uglify())
   .pipe(dest('dist/js'))


const convertImg = () => src('src/img/**/*')
  .pipe(imageMin())
  .pipe(dest('dist/img'))

const startWatching = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    watch('src/sass/**/*').on('all', series(convertCss, browserSync.reload));
    watch('src/js/**/*').on('all', series(convertJs, browserSync.reload));
    watch('src/img/**/*').on('all', series(convertImg, browserSync.reload));
}

function build(done) {
    return gulp.series(cleanDist, convertCss, convertJs, convertImg)(done);  
}

task('build', build );
task('dev', startWatching);












// import gulp from 'gulp';
// import dartSass from 'sass';
// import gulpSass from 'gulp-sass';
// import concat from 'gulp-concat';
// import { del } from 'del';
// import uglify from 'gulp-uglify';
// import cleanCSS from 'gulp-clean-css';
// import rename from 'gulp-rename';
// import imageMin from 'gulp-imagemin';
// import { create as bsCreate } from 'browser-sync';
// import autoPrefixer from 'gulp-autoprefixer';
// const browserSync = bsCreate();
// const sass = gulpSass(dartSass);

// gulp.task('css_build', function () {
// 	return gulp
// 		.src('./src/scss/main.scss')
// 		.pipe(sass.sync().on('error', sass.logError))
// 		.pipe(cleanCSS())
//         .pipe(autoPrefixer('last 2 version'))
// 		.pipe(rename('main.min.css'))
// 		.pipe(gulp.dest('dist/css'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('compress_img', function () {
// 	return gulp
// 		.src('src/img/**')
// 		.pipe(
// 			imageMin({
// 				progressive: true,
// 			})
// 		)
// 		.pipe(gulp.dest('dist/img'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('js_build', function () {
// 	return gulp
// 		.src('src/js/**/*.js')
// 		.pipe(concat('scripts.min.js'))
// 		.pipe(uglify())
// 		.pipe(gulp.dest('dist/js'))
// 		.pipe(browserSync.reload({ stream: true }));
// });

// gulp.task('clean_dist', function () {
// 	return del('dist/**', { force: true });
// });

// gulp.task(
// 	'build',
// 	gulp.series('clean_dist', gulp.parallel(['css_build', 'js_build', 'compress_img']))
// );


// gulp.task('webServer', function () {
// 	browserSync.init({
// 		server: './',
// 	});
// 	gulp.watch('src/**/*.scss', gulp.series('css_build'));
// 	gulp.watch('src/**/*.js', gulp.series('js_build'));
// 	gulp.watch('src/img/**', gulp.series('compress_img'));
// });

// gulp.task('default', gulp.series(['build', 'webServer']));
