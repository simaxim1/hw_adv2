const menu = document.querySelector(".menu-hidden");
const menuWrap = document.querySelector(".header__menu-wrapper");
const menuLink = document.querySelectorAll(".header__menu-link");

const openMenu = () => {
    menu.classList.toggle("active");
    menuWrap.classList.toggle("active");
}

const hideMenu = () => {
    menu.classList.remove("active");
    menuWrap.classList.remove("active");
}

menu.addEventListener("click", openMenu);
menuLink.forEach(e => e.addEventListener("click", hideMenu));